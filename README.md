# Getting started

⚠️ This texture pack is made for **1.18.1** and need **[Optifine](https://optifine.net/downloads)** ⚠️

For the installation, just download the repo as zip and unzip it into your *.minecraft/resourcepacks* folder

# Want more ?

You can get the **Mob Sound Addon [here](https://mega.nz/file/pOxSlDCD#NMhkfa54GzAnD2mPPEtlSEW_3ZXxsvdF1O85fHBXUMk)**

## Infos

Based on :
 - [Stay True](https://www.curseforge.com/minecraft/texture-packs/stay-true)
 - [xali's Potions](https://www.curseforge.com/minecraft/texture-packs/xalis-potions)
 - [xali's Enchanted Books](https://www.curseforge.com/minecraft/texture-packs/xalis-enchanted-books)
 - [Rainbow XP bar and ping](https://www.curseforge.com/minecraft/texture-packs/rainbow-xp-bar-and-ping)
 - [BetterEnderDragon](https://www.curseforge.com/minecraft/texture-packs/better-ender-dragon)
 - [GUI Retextures](https://www.curseforge.com/minecraft/texture-packs/gui-retextures)
 - [Dramatic skys](https://www.curseforge.com/minecraft/texture-packs/dramatic-skys)
